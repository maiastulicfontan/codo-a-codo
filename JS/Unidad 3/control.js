
const formularioIngreso = document.getElementById("formulario-ingreso");

formularioIngreso.addEventListener("submit", function (event) {
  const usuario = document.getElementById("usuario").value;
  const clave = document.getElementById("clave").value;
  let errores = [];
  if (usuario.length === 0) {
    errores.push("El campo 'Usuario' no debe estar vacío");
  } else if (!usuario.includes("@")) {
    errores.push("El campo 'Usuario' debe contener por lo menos un caracter '@'");
  }
  if (clave.length === 0) {
    errores.push("El campo 'Clave' no debe estar vacío");
  }
  if (errores.length !== 0) {
    event.preventDefault();
    alert(errores.join("\n"));
  } else {
    formularioIngreso.submit();
    alert("Formulario enviado");
  }
})
